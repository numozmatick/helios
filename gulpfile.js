const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const watch = require('gulp-watch');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync');

const cssFiles =[
    './node_modules/normalize.css/normalize.css',
    './src/scss/slick.scss',
    './src/scss/main.scss',
    './src/scss/font.scss',
    './src/scss/mixins.scss',
    './src/scss/style.scss',
    './src/scss/variables.scss',
    './src/scss/templates/bl-container.scss',
    './src/scss/templates/link.scss',
    './src/scss/templates/wrapper-grid.scss',
    './src/scss/templates/button.scss',
    './src/scss/templates/modal.scss',
    //block home
    './src/scss/block/top.scss',
    './src/scss/block/header.scss',
    './src/scss/block/ceil.scss',
    './src/scss/block/footer.scss',
    './src/scss/block/banner.scss',
    './src/scss/block/service.scss',
    './src/scss/block/banner-house.scss',
    './src/scss/block/project.scss',
    './src/scss/block/our-projects.scss',
    './src/scss/block/comment.scss',
    './src/scss/block/brand.scss',
    //buyer
    './src/scss/block/buyer/banner-price.scss',
    './src/scss/block/buyer/order-service.scss',
    './src/scss/block/buyer/inquirer.scss',
    './src/scss/block/buyer/profile-Veka.scss',
    './src/scss/block/buyer/quality.scss',
    './src/scss/block/buyer/item-cards.scss',
    './src/scss/block/buyer/pages.scss',
    './src/scss/block/buyer/timer.scss',
    './src/scss/block/buyer/service-buyer.scss',
    './src/scss/block/buyer/banner-buyer.scss',
    './src/scss/block/buyer/procedure.scss',
    './src/scss/block/buyer/produced.scss',
    './src/scss/block/buyer/map.scss',
    './src/scss/block/buyer/invite.scss',
    './src/scss/block/buyer/buyer-brand.scss',
    './src/scss/block/buyer/order-call.scss',
    './src/scss/block/buyer/comment-buyer.scss',
    './src/scss/block/buyer/confirmation.scss',
    './src/scss/block/buyer/chat.scss',
    './src/scss/block/buyer/certificate.scss',
    //media
    './src/scss/_media.scss',




];
const jsFiles = [

];
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        browser: 'google chrome',
        notify: false
    });
});
gulp.task('sass-compile', function () {
    return gulp.src(cssFiles)
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('all.css'))
        .pipe(sourcemaps.init())
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cleanCSS({level: 2}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('src/build/css'))
        .pipe(browserSync.stream())
})

gulp.task('js-compile', function () {
    return gulp.src(jsFiles)
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./src/build/js'))
})


gulp.task('serve', function() {

    browserSync.init({
        server: "./"
    });
    gulp.watch('src/scss/**/*.scss', gulp.series('sass-compile'))
    gulp.watch("./index.html").on('change', browserSync.reload);
});
