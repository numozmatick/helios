$(document).ready(function () {
	$('.slider--brand').slick({
		arrows: false,
		dots: true,
		slidesToShow: 5,
		slidesToScroll: 2,
		speed: 1000,
		draggable: true,
		infinite: true,
		centerMode: true,

		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 550,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});


	$('.slider--comment').slick({
		arrows: false,
		dots: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		speed: 1000,
		draggable: true,
		infinite: false,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 550,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});

	$('.slider--project').slick({
		arrows: false,
		dots: true,
		slidesToShow: 4,
		slidesToScroll: 2,
		speed: 1000,
		draggable: true,

		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 550,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			}
		]
	});

	$('.slider--banner').slick({
		arrows: true,
		dots: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 1000,
		draggable: true,

		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 1
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1
				}
			},
			{
				breakpoint: 550,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});
	$('.slider--inquirer-dots').slick({
		arrows: false,
		dots: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 1000,
		infinite: false,
		draggable: false,
		asNavFor: '.slider--inquirer',
		centerPadding: 0,

	});

	$('.slider--inquirer').slick({
		arrows: false,
		dots: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 1000,
		infinite: false,
		draggable: false,
		asNavFor: '.slider--inquirer-dots',
		centerPadding: 0,

	});

	//buyer
	$('.slider--buyer-brand').slick({
		arrows: false,
		dots: true,
		slidesToShow: 5,
		slidesToScroll: 5,
		speed: 1000,
		draggable: true,
		infinite: true,
		centerMode: true,

		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToScroll: 2,
					slidesToShow: 2
				}
			},
			{
				breakpoint: 550,
				settings: {
					slidesToScroll: 1,
					slidesToShow: 1
				}
			}
		]
	});

	$('.slider--certificate').slick({
		arrows: false,
		dots: true,
		slidesToShow: 5,
		slidesToScroll: 5,
		speed: 1000,
		draggable: true,

		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToScroll: 2,
					slidesToShow: 2
				}
			},
			{
				breakpoint: 550,
				settings: {
					slidesToScroll: 1,
					slidesToShow: 1
				}
			}
		]
	});




	$('.comment__detailed').click(function () {
		const commentTextElem = $(this).parent().children('.comment__text');
		if (commentTextElem.css("height") !== "195px") {
			commentTextElem.css("height", "195px")
		} else {
			commentTextElem.css("height", "auto")
			commentTextElem.css("height", "visible")
		}
		const commentDetailElem = $(this).parent().children('.comment__detailed');
		if ($('.comment__text').css("height") > "195px") {
			$(commentDetailElem).css('display',"none")
		}else{
			$(commentDetailElem).css('display',"block")
		}
	});

	$('.item-card__subtitle-open').click(function () {
		$(this).parent().children('.item-card__subtitle').slideToggle(300, function () {
			if ($(this).css('display') === "none") {
				$(this).removeAttr('style');
			}
		})
	})

	$('.menu__item--footer').click(function () {
		$(this).children('.menu__down--footer').slideToggle(300, function () {

			if ($(this).css('display') === "none") {
				$(this).removeAttr('style');
			}
		})
		if ($(this).children('.fa-caret-right').css("transform") === "none") {
			$(this).children('.fa-caret-right').css("transform", "rotate(90deg)")
		} else {
			$('.fa-caret-right').removeAttr('style');
		}


	})

	$('.map__city').click(function () {
		if ($(this).children('.fa-caret-up').css("transform") === "none") {
			$(this).children('.fa-caret-up').css("transform", "rotate(180deg)")
		} else {
			$('.fa-caret-up').removeAttr('style');
		}


	})


	$('.map__city').click(function () {
		$('.map__about-wrapper').slideToggle(function () {
			if ($(this).css('display') === "block") {
				$(this).removeAttr('style');
			}
		})
	})
	$('.map__menu-item').click(function () {
		$('.map__menu-item').removeClass ('active-city')
		$(this).addClass ('active-city')
	})

		$('article').readmore({ //вызов плагина
			speed: 250, //скорость раскрытия скрытого текста (в миллисекундах)
			maxHeight: 182, //высота раскрытой области текста (в пикселях)
			heightMargin: 16, //избегание ломания блоков, которые больше maxHeight (в пикселях)
			moreLink: '<a href="#">Читать далее</a>', //ссылка "Читать далее", можно переименовать
			lessLink: '<a href="#">Скрыть</a>' //ссылка "Скрыть", можно переименовать
		});

		window.onscroll = function showHeader() {
		var header = document.querySelector('.bl-container--header');
		if(window.pageYOffset > 200){
			header.classList.add('header__fixed');
		} else{
			header.classList.remove('header__fixed');
		}
	}
});


